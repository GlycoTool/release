# GlycanBuilder2 for Windows x64

## Usage

* Command Prompt (cmd.exe)

```
>java -jar Glycanbuilder2_winx64.jar
```
* GUI

  * Double-click the Glycanbuilder2_winx64.jar.


## Windows

* OS: Windows 10 Home (64bit) or Windows 10 Pro *64bit()
* version: 1909 or 20H2
* OS build: 18363.1256 or 19042.985

* CPU: Intel Pentium CPU 4415Y @ 1.60GHz 1.61 or Intel Core i3-4020Y @1.50GHz1.50
* RAM: 8GB or 4GB

## java
* JDK: http://jdk.java.net/16/
  * https://download.java.net/java/GA/jdk16.0.1/7147401fd7354114ac51ef3e1328291f/9/GPL/openjdk-16.0.1_windows-x64_bin.zip



## Windows 10 Environment Variables

* Variables: JAVA_HOME
* Value: C:\jdk-16.0.1

* Variables: Path
* Value:  %JAVA_HOME%\bin


```
Microsoft Windows [Version 10.0.18363.1256]
(c) 2019 Microsoft Corporation. All rights reserved.

C:\Users\yamad>java -version
openjdk version "16.0.1" 2021-04-20
OpenJDK Runtime Environment (build 16.0.1+9-24)
OpenJDK 64-Bit Server VM (build 16.0.1+9-24, mixed mode, sharing)

C:\Users\yamad>mvn -v
Apache Maven 3.8.1 (05c21c65bdfed0f71a2f2ada8b84da59348c4c5d)
Maven home: C:\apache-maven-3.8.1\bin\..
Java version: 16.0.1, vendor: Oracle Corporation, runtime: C:\jdk-16.0.1
Default locale: ja_JP, platform encoding: MS932
OS name: "windows 10", version: "10.0", arch: "amd64", family: "windows"
```

or

```
Microsoft Windows [Version 10.0.19042.985]
(c) Microsoft Corporation. All rights reserved.

>java -version
java version "1.8.0_121"
Java(TM) SE Runtime Environment (build 1.8.0_121-b13)
Java HotSpot(TM) 64-Bit Server VM (build 25.121-b13, mixed mode)
```


```
>java -jar Glycanbuilder2_winx64.jar
```

## maven

* https://maven.apache.org/download.cgi
  * https://ftp.tsukuba.wide.ad.jp/software/apache/maven/maven-3/3.8.1/binaries/apache-maven-3.8.1-bin.zip
