# wurcs2pic

* version 0.10.0


# wurcs2pic build condition

```
$ java -version
openjdk version "13.0.4" 2020-07-14
OpenJDK Runtime Environment (build 13.0.4+8-Ubuntu-120.04)
OpenJDK 64-Bit Server VM (build 13.0.4+8-Ubuntu-120.04, mixed mode, sharing)
```

```
$ cat /etc/os-release
NAME="Ubuntu"
VERSION="20.04.1 LTS (Focal Fossa)"
ID=ubuntu
ID_LIKE=debian
PRETTY_NAME="Ubuntu 20.04.1 LTS"
VERSION_ID="20.04"
HOME_URL="https://www.ubuntu.com/"
SUPPORT_URL="https://help.ubuntu.com/"
BUG_REPORT_URL="https://bugs.launchpad.net/ubuntu/"
PRIVACY_POLICY_URL="https://www.ubuntu.com/legal/terms-and-policies/privacy-policy"
VERSION_CODENAME=focal
UBUNTU_CODENAME=focal
```

```
$ gradle -v

------------------------------------------------------------
Gradle 6.8.3
------------------------------------------------------------

Build time:   2021-02-22 16:13:28 UTC
Revision:     9e26b4a9ebb910eaa1b8da8ff8575e514bc61c78

Kotlin:       1.4.20
Groovy:       2.5.12
Ant:          Apache Ant(TM) version 1.10.9 compiled on September 27 2020
JVM:          13.0.4 (Private Build 13.0.4+8-Ubuntu-120.04)
OS:           Linux 5.4.0-42-generic amd64
```
