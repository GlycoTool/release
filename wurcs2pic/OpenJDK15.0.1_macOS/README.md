# wurcs2pic

* version 0.10.1

# wurcs2pic build condition

```
$ java -version
openjdk version "15.0.1" 2020-10-20
OpenJDK Runtime Environment (build 15.0.1+9)
OpenJDK 64-Bit Server VM (build 15.0.1+9, mixed mode, sharing)
```

```
$ sw_vers
ProductName:	macOS
ProductVersion:	11.2.3
BuildVersion:	20D91
```

```
$ gradle -v

------------------------------------------------------------
Gradle 6.8.2
------------------------------------------------------------

Build time:   2021-02-05 12:53:00 UTC
Revision:     b9bd4a5c6026ac52f690eaf2829ee26563cad426

Kotlin:       1.4.20
Groovy:       2.5.12
Ant:          Apache Ant(TM) version 1.10.9 compiled on September 27 2020
JVM:          15.0.1 (Oracle Corporation 15.0.1+9)
OS:           Mac OS X 10.16 x86_64
```



