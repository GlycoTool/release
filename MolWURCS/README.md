# MolWURCS

# MolWURCS version and help

```
[glyconavi@calc target]$ java -jar MolWURCS.jar -h                                                                           
usage: MolWURCS  [--with-aglycone] [--title-property-id <PROPERTY_ID>]
                --in <FORMAT> --out <FORMAT> [--output-no-result]
                [--double-check][<INPUT FORMAT STRING>]

Options:
    --double-check
 Double-check output WURCS using WURCS to WURCS convert, not active when
 "wurcs" is set to input and output formats at the same time
 -h,--help
 Show usage help
 -i,--in <FORMAT=[wurcs|mdlv2000|mdlv3000|sdf|mol2|pdb|cml|xyz|smi|inchi]>
 Set input format, required
 -n,--output-no-result
 Output result explicitly even if the conversion is failed or not
 contained result
 -o,--out <FORMAT=[wurcs|mdlv2000|sdf|mol2|pdb|cml|xyz|smi]>
 Set output format, required
 -p,--title-property-id <PROPERTY_ID>
 Use property value as title, which key of the value is <PROPERTY_ID>
 -r,--report
 Output glycan extraction report, active only when "wurcs" is specified as
 output format
    --with-aglycone
 Output WURCS with aglycone, active only when "wurcs" is specified as
 output format

Version: 0.7.4
```

## Repository

https://gitlab.com/glycoinfo/molwurcs

ae534d4b7cee7bffbd919e42185897db4162accc



## Usage 

```
$ cat CID5958.sdf | java -jar ./MolWURCS.jar --double-check -n -i sdf -o wurcs -p PUBCHEM_COMPOUND_CID 2>/dev/null
```

* result
```
5958	WURCS=2.0/1,1,0/[a2122h-1x_1-5_6*OPO/3O/3=O]/1/
```



## Usage 1
```
$ gzcat input.sdf.gz | java -jar MolWURCS.jar -i sdf -o wurcs
```

## Usage 2
```
$ gzcat input.sdf.gz | java -jar MolWURCS.jar -i sdf -o wurcs 1> ./result/std.out.log 2> ./result/std.err.log 
```

## Help
```
$ java -jar MolWURCS.jar -h
```





