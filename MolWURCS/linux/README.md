# build

* https://gitlab.com/glycoinfo/molwurcs

## conditions
```
$ cat /etc/redhat-release
CentOS Linux release 7.9.2009 (Core)
$ java -version
openjdk version "1.8.0_302"
OpenJDK Runtime Environment (build 1.8.0_302-b08)
OpenJDK 64-Bit Server VM (build 25.302-b08, mixed mode)
$ mvn --version
Apache Maven 3.8.3 (ff8e977a158738155dc465c6a97ffaf31982d739)
Maven home: /opt/apache-maven
Java version: 1.8.0_302, vendor: Red Hat, Inc., runtime: /usr/lib/jvm/java-1.8.0-openjdk-1.8.0.302.b08-0.el7_9.x86_64/jre
Default locale: ja_JP, platform encoding: UTF-8
OS name: "linux", version: "3.10.0-1160.el7.x86_64", arch: "amd64", family: "unix"
```
## build

```
$ git clone https://gitlab.com/glycoinfo/molwurcs.git
$ cd molwurcs
$ mvn clean compile assembly:single
```
