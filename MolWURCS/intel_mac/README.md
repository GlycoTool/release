# build

* https://gitlab.com/glycoinfo/molwurcs

## conditions
```
$ sw_vers
ProductName:	macOS
ProductVersion:	11.6.1
BuildVersion:	20G224

$ java -version
openjdk version "17.0.1" 2021-10-19
OpenJDK Runtime Environment (build 17.0.1+12-39)
OpenJDK 64-Bit Server VM (build 17.0.1+12-39, mixed mode, sharing)

$ mvn --version
Apache Maven 3.6.3 (cecedd343002696d0abb50b32b541b8a6ba2883f)
Maven home: /usr/local/apache-maven-3.6.3
Java version: 17.0.1, vendor: Oracle Corporation, runtime: /Library/Java/JavaVirtualMachines/jdk-17.0.1.jdk/Contents/Home
Default locale: ja_JP, platform encoding: UTF-8
OS name: "mac os x", version: "11.6.1", arch: "x86_64", family: "mac"
```
## build

```
$ git clone https://gitlab.com/glycoinfo/molwurcs.git
$ cd molwurcs
$ mvn clean compile assembly:single
```
