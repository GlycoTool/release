#

```
$ mvn -version
Apache Maven 3.0.5 (Red Hat 3.0.5-17)
Maven home: /usr/share/maven
Java version: 1.8.0_302, vendor: Red Hat, Inc.
Java home: /usr/lib/jvm/java-1.8.0-openjdk-1.8.0.302.b08-0.el7_9.x86_64/jre
Default locale: ja_JP, platform encoding: UTF-8
OS name: "linux", version: "3.10.0-1160.el7.x86_64", arch: "amd64", family: "unix"
$ java -version
openjdk version "1.8.0_302"
OpenJDK Runtime Environment (build 1.8.0_302-b08)
OpenJDK 64-Bit Server VM (build 25.302-b08, mixed mode)
$ cat /etc/redhat-release 
CentOS Linux release 7.9.2009 (Core)
````

# build
```
$ git clone https://gitlab.com/glycoinfo/molwurcs.git
$ mvn clean compile assembly:single
```

```
$ java -jar ./target/MolWURCS.jar -h
usage: MolWURCS  [--with-aglycone] [--title_property_id <PROPERTY_ID>]
                --in <FORMAT> --out <FORMAT>[<INPUT FORMAT STRING>]

Options:
 -h,--help
 Show usage help
 -i,--in <FORMAT=[wurcs|mdlv2000|mdlv3000|sdf|mol2|pdb|cml|xyz|smi|inchi]>
 Set input format, required
 -o,--out <FORMAT=[wurcs|mdlv2000|sdf|mol2|pdb|cml|xyz|smi]>
 Set output format, required
 -p,--title_property_id <PROPERTY_ID>
 Use property value as title, which key of the value is <PROPERTY_ID>
    --with-aglycone
 Output WURCS with aglycone, active only when "wurcs" is specified as
 output format

Version: 0.5.0
```
